export const voteReact = () => {
    return { type: "VOTE_REACT" }
}
export const voteAngular = () => {
    return { type: "VOTE_ANGULAR" }
}
export default  () => {
    return { type: "VOTE_JQUERY" }
}