
const initailState = { angular: 0, react: 0, jquery: 0 };


export default (state = initailState, action) => {

    switch (action.type) {
        case "VOTE_ANGULAR": return GetNewObject(state, "angular");
        case "VOTE_REACT": return GetNewObject(state, "react");
        case "VOTE_JQUERY": return GetNewObject(state, "jquery");
        default: state
    }
}

function GetNewObject(state, prop) {
    return Object.assign({}, state, {
        [prop]: state[prop] + 1
    })
}