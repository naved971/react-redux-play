import React from 'react'
import ReactDOM from 'react-dom'

export class Result extends React.Component {
    constructor(props) {
        super(props);
        debugger;
        this.store = this.props.store;
        this.state = this.store.getState() || {};
        ["getAngular", "getReact", "getJquery"].map(x => this[x].bind(this));
    }
    getAngular() {
        if (this.store.getState())
            return this.store.getState().angular;
        else
            return 0;
    }
    getJquery() {
        if (this.store.getState())
            return this.store.getState().jquery;
        else
            return 0;
    }
    getReact() {
        if (this.store.getState())
            return this.store.getState().react;
        else
            return 0;
    }
    render() {
        return (
            <div>
                <h1>From Result</h1>

                <h1> Angular  : {this.getAngular()}</h1>
                <h1> React  : {this.getJquery()}</h1>
                <h1> Jquery  : {this.getReact()}</h1>
            </div>
        )
    }
}
