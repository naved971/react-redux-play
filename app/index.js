import React from 'react'
import ReactDOM from 'react-dom'
import { createStore } from 'redux'
import myAppReducer from './reducers'
import voteJquery, { voteAngular, voteReact } from './action'
import { Result } from './result'
import './App.css'
let store = createStore(myAppReducer);

export class App extends React.Component {
    constructor(props) {
        super(props);
        this.store = this.props.store;
        
    }
    voteAppAngular() {
        this.store.dispatch(voteAngular());
    }
    voteAppJquery() {
        this.store.dispatch(voteJquery());
    }
    voteAppReact() {
        this.store.dispatch(voteReact());
    }
    render() {
        return (
            <div >

                <h1>Hello from Docker Demo</h1>
                <div className="jumbotron"></div>
                <br />
                <div className="row">

                    <div className="col-xs-offset-3 col-xs-2">
                        <img src="./images/jquery.png" width="100" height="100"  onClick={()=>{this.voteAppJquery();}} />
                    </div>
                    <div className="col-xs-2">
                        <img src="./images/angular.jpg" width="100" height="100" onClick={()=>{this.voteAppAngular();}}  />
                    </div>
                    <div className="col-xs-2">
                        <img src="./images/react.png" width="150" height="100"  onClick={()=>{this.voteAppReact();}} />
                    </div>

                </div>
            </div>
        )
    }
}


function render() {

    ReactDOM.render(
        (<div id="container">
            <App store={store} />
            <hr />
            <Result store={store} />
        </div>)
        , document.getElementById('root'));
}
store.subscribe(render);
render();

