var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var BUILD_APP = path.resolve(__dirname, "public");
var APP_DIR = path.resolve(__dirname, "app");

var config = {
    entry: APP_DIR + "/index.js",
    output: {
        path: BUILD_APP,
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js|jsx?/,
                include: APP_DIR,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([
            { from: 'app/images', to: 'images' }
        ]),
    ]

}
module.exports = config

//babel-core babel-loader babel-preset-es2015 babel-preset-react